FROM node:lts
RUN mkdir -p /code
WORKDIR /code
ADD . /code
RUN yarn install
RUN yarn build
RUN yarn global add serve
COPY . .
CMD serve -s -l 80 dist
EXPOSE 80