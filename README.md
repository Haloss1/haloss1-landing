# Haloss1 personal landing page
## How To Use

```bash
# Clone this repository
$ git clone https://gitlab.com/Haloss1/haloss1-landing

# Go into the repository
$ cd haloss1-landing

# Install dependencies
$ yarn install

# Start development server
$ yarn start
```
